<?php

declare(strict_types=1);

use Cake\Core\Plugin;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use \Cake\Routing\RouteBuilder;

Router::defaultRouteClass(DashedRoute::class);
Router::extensions(['json']);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->fallbacks(DashedRoute::class);
});

Plugin::routes();